package com.chords;

import com.core.Chord;
import com.core.Scale;
import com.core.ScaleHelper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Service {
    String findChords(String key) {
        List<Scale> scales = ScaleHelper.getScales(key);
        List<Chord> chords = new ArrayList<>();
        for (Scale scale : scales) {
            List<Chord> temporaryChords = scale.getChords();
            for (Chord temporaryChord : temporaryChords) {
                boolean chordPresent = false;
                for (Chord chord : chords) {
                    if (chord.getRootNote().equals(temporaryChord.getRootNote()) && chord.getType().equals(temporaryChord.getType())) {
                        chord.setScale(chord.getScale() + ", " + temporaryChord.getScale());
                        chordPresent = true;
                    }
                }
                if (!chordPresent) {
                    chords.add(temporaryChord);
                }
            }
        }
        chords.sort(Comparator.comparing(Chord::getRootNote));
        StringBuilder result = new StringBuilder();
        for (Chord chord : chords) {
            result.append(chord.getRootNote())
                    .append(" ")
                    .append(chord.getType())
                    .append(" - ")
                    .append(chord.getScale())
                    .append("\n");
        }

        return result.toString();
    }
}
