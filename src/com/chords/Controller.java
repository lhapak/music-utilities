package com.chords;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

public class Controller {
    public Label chords;
    public ChoiceBox key;
    private Service service = new Service();

    public void findChords() {
        chords.setText(service.findChords(key.getValue().toString()));
    }
}
