package com.scales;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {
    public Label labelScales;
    public ChoiceBox note1;
    public ChoiceBox note2;
    public ChoiceBox note3;
    public ChoiceBox note4;
    public ChoiceBox note5;
    public ChoiceBox note6;
    public ChoiceBox note7;
    private List<ChoiceBox> selectedNotes = new ArrayList<>();

    private Service service = new Service();

    @FXML
    public void initialize() {
        selectedNotes.add(note1);
        selectedNotes.add(note2);
        selectedNotes.add(note3);
        selectedNotes.add(note4);
        selectedNotes.add(note5);
        selectedNotes.add(note6);
        selectedNotes.add(note7);
    }

    public void findScales() {
        List<String> notes = new ArrayList<>();
        for (ChoiceBox note : selectedNotes) {
            if (note.getValue() != null) {
                notes.add(note.getValue().toString());
            }
        }
        notes = notes.stream().filter(x -> !x.equals("")).collect(Collectors.toList());
        if (notes.size() > 2) {
            String scales = service.findScales(notes);
            labelScales.setText(scales);
        } else {
            labelScales.setText("");
        }
    }
}
