package com.scales;

import com.core.Scale;
import com.core.ScaleHelper;

import java.util.List;
import java.util.stream.Collectors;

class Service {

    String findScales(List<String> notes) {
        List<Scale> scales = ScaleHelper.getScales();
        scales = scales.stream().filter(x -> x.getScaleNotes().containsAll(notes)).collect(Collectors.toList());
        StringBuilder result = new StringBuilder();
        for (Scale scale : scales) {
            result.append(scale.getKey()).append(" ").append(scale.getName()).append("\n");
        }
        if (result.toString().equals("")) {
            return "no scales found!";
        }
        return result.toString();
    }
}
