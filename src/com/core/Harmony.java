package com.core;

public class Harmony {
    private String note;
    private int interval;
    private String scale;

    Harmony(String note, int interval, String scale) {
        this.note = note;
        this.interval = interval;
        this.scale = scale;
    }

    public String getNote() {
        return note;
    }

    public int getInterval() {
        return interval;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }
}
