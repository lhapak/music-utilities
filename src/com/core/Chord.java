package com.core;

import java.util.List;

public class Chord {
    private String rootNote;
    private String scale;
    private String type;

    Chord(String rootNote, String scale, List<Integer> intervals) {
        this.rootNote = rootNote;
        this.scale = scale;
        type = getChordType(intervals);
    }

    public String getRootNote() {
        return rootNote;
    }

    public String getScale() {
        return scale;
    }

    public String getType() {
        return type;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    private String getChordType(List<Integer> intervals) {
        if (intervals.contains(4) && intervals.contains(7)) {
            return "Major";
        } else if (intervals.contains(3) && intervals.contains(7)) {
            return "Minor";
        }  else if (intervals.contains(3) && intervals.contains(6)) {
            return "Diminished";
        } else if (intervals.contains(4) && intervals.contains(8)) {
            return "Augmented";
        } else {
            return "unknown";
        }
    }
}
