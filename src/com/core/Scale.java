package com.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Scale {
    private String name;
    private List<String> pattern;
    private String key;
    private List<String> scaleNotes = new ArrayList<>();
    private static List<String> allNotes = new ArrayList<>(Arrays.asList("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"));

    Scale(String name, String pattern, String key) {
        this.name = name;
        this.pattern = Arrays.asList(pattern.split(" "));
        this.key = key;
        getNotes();
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public List<String> getScaleNotes() {
        return scaleNotes;
    }

    static List<String> getAllNotes() {
        return allNotes;
    }

    public List<Harmony> getHarmonies(String note) {
        List<Harmony> harmonies = new ArrayList<>();
        int position;
        if (scaleNotes.contains(note)) {
            position = scaleNotes.indexOf(note);
            position++;
            position %= scaleNotes.size();

            for (int i = 0; i < scaleNotes.size() - 1; i++) {
                harmonies.add(new Harmony(scaleNotes.get(position), getInterval(note, scaleNotes.get(position)), name));
                position++;
                position %= scaleNotes.size();
            }
        }
        return harmonies;
    }

    public List<Chord> getChords() {
        List<Chord> chords = new ArrayList<>();
        for (String note : scaleNotes) {
            chords.add(new Chord(note, name, getChordIntervals(note)));
        }
        return chords;
    }

    private void getNotes() {
        int position = allNotes.indexOf(key);

        for (String step : pattern) {
            scaleNotes.add(allNotes.get(position));

            switch (step) {
                case "H":
                    position++;
                    break;
                case "W":
                    position += 2;
                    break;
                case "3H":
                    position += 3;
                    break;
            }
            position %= allNotes.size();
        }
    }

    private int getInterval(String firstNote, String secondNote) {
        int interval = 0;
        int position = allNotes.indexOf(firstNote);

        while (!(allNotes.get(position).equals(secondNote))) {
            position++;
            position %= allNotes.size();
            interval++;
        }
        return interval;
    }

    private List<Integer> getChordIntervals(String note) {
        List<Integer> intervals = new ArrayList<>();
        int position;
        if (scaleNotes.contains(note)) {
            position = scaleNotes.indexOf(note);
            position += 2;
            position %= scaleNotes.size();

            for (int i = 0; i < 2; i++) {
                intervals.add(getInterval(note, scaleNotes.get(position)));
                position += 2;
                position %= scaleNotes.size();
            }
        }
        return intervals;
    }
}
