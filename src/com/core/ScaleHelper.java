package com.core;

import java.util.ArrayList;
import java.util.List;

public class ScaleHelper {
    private static List<Scale> scales = new ArrayList<>();

    public static List<Scale> getScales(String key) {
        scales.clear();
        createScalesObjects(key);
        return scales;
    }

    public static List<Scale> getScales() {
        scales.clear();
        for (String note : Scale.getAllNotes()) {
            createScalesObjects(note);
        }
        return scales;
    }

    private static void createScalesObjects(String key) {
        scales.add(new Scale("Ionian", "W W H W W W H", key));
        scales.add(new Scale("Dorian", "W H W W W H W", key));
        scales.add(new Scale("Phrygian", "H W W W H W W", key));
        scales.add(new Scale("Lydian", "W W W H W W H", key));
        scales.add(new Scale("Mixolydian", "W W H W W H W", key));
        scales.add(new Scale("Aeolian", "W H W W H W W", key));
        scales.add(new Scale("Locrian", "H W W H W W W", key));
        scales.add(new Scale("Harmonic minor", "W H W W H 3H H", key));
        scales.add(new Scale("Melodic minor", "W H W W W W H", key));
    }
}
