package com.harmonies;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import java.util.List;

public class Controller {
    public ChoiceBox note;
    public ChoiceBox key;
    public Label consonant;
    public Label dissonant;
    public Label perfect;

    private Service service = new Service();

    public void findHarmony() {
        List<String> harmonies = service.findHarmonies(note.getValue().toString(), key.getValue().toString());
        consonant.setText(harmonies.get(0));
        dissonant.setText(harmonies.get(1));
        perfect.setText(harmonies.get(2));
    }
}
