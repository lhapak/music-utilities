package com.harmonies;

import com.core.Harmony;
import com.core.Scale;
import com.core.ScaleHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Service {
    private List<Integer> dissonantIntervals = new ArrayList<>(Arrays.asList(1, 2, 6, 10, 11));
    private List<Integer> consonantIntervals = new ArrayList<>(Arrays.asList(3, 4, 8, 9));
    private List<Integer> perfectIntervals = new ArrayList<>(Arrays.asList(5, 7, 12));
    private List<Harmony> harmonies;

    List<String> findHarmonies(String note, String key) {
        harmonies = getAllHarmoniesList(note, key);
        List<String> result = new ArrayList<>();
        harmonies.sort(Comparator.comparing(Harmony::getInterval));

        result.add(getHarmonies(consonantIntervals));
        result.add(getHarmonies(dissonantIntervals));
        result.add(getHarmonies(perfectIntervals));

        return result;
    }

    private String getHarmonies(List<Integer> intervals) {
        List<Harmony> specifiedHarmonies = harmonies.stream()
                .filter(x -> intervals.contains(x.getInterval()))
                .collect(Collectors.toList());
        StringBuilder result = new StringBuilder();
        for (Harmony harmony : specifiedHarmonies) {
            result.append(harmony.getNote())
                    .append(" - ")
                    .append(getIntervalInWords(harmony.getInterval()))
                    .append(" - ")
                    .append(harmony.getScale())
                    .append("\n");
        }
        return result.toString();
    }

    private List<Harmony> getAllHarmoniesList(String note, String key) {
        List<Scale> scales = ScaleHelper.getScales(key);
        List<Harmony> result = new ArrayList<>();
        for (Scale scale : scales) {
            List<Harmony> temporaryHarmonies = new ArrayList<>(scale.getHarmonies(note));

            for (Harmony temporaryHarmony : temporaryHarmonies) {
                boolean harmonyPresent = false;
                for (Harmony harmony : result) {
                    if (harmony.getNote().equals(temporaryHarmony.getNote())) {
                        harmony.setScale(harmony.getScale() + ", " + temporaryHarmony.getScale());
                        harmonyPresent = true;
                    }
                }
                if (!harmonyPresent) {
                    result.add(temporaryHarmony);
                }
            }
        }
        return result;
    }

    private String getIntervalInWords(int interval) {
        switch (interval) {
            case 1:
                return "Minor Second";
            case 2:
                return "Major Second";
            case 3:
                return "Minor Third";
            case 4:
                return "Major Third";
            case 5:
                return "Perfect Fourth";
            case 6:
                return "Diminished Fifth";
            case 7:
                return "Perfect Fifth";
            case 8:
                return "Minor Sixth";
            case 9:
                return "Major Sixth";
            case 10:
                return "Minor Seventh";
            case 11:
                return "Major Seventh";
            default:
                return "Octave";
        }
    }
}
