package com.notes;

import com.core.Scale;
import com.core.ScaleHelper;

import java.util.List;
import java.util.stream.Collectors;

class Service {

    String findNotes(String key) {
        return getScaleNotes(ScaleHelper.getScales(key));
    }

    private String getScaleNotes(List<Scale> scales) {
        StringBuilder result = new StringBuilder();
        for (Scale scale : scales) {
            result.append(scale.getName()).append(": ");
            List<String> notes = scale.getScaleNotes();
            result.append(notes.stream().collect(Collectors.joining(", ")));
            result.append("\n");
        }
        return result.toString();
    }
}
