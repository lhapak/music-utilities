package com.notes;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

public class Controller {
    public Label notes;
    public ChoiceBox key;

    private Service service = new Service();

    public void findNotes() {
        notes.setText(service.findNotes(key.getValue().toString()));
    }
}
