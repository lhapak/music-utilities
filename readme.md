# Music utilities

Simple window application made using Java fx

## Features:

### Scale Notes:

```
user can select key for scales and application will diplay notes in various scales that fits the key 
```

### Find Scale:

```
app will find scales that contains all selected notes
```

### Harmony:

```
displays intervals for selected note among scales created in given key, intervals are grouped by type,
 there is also information which scales includes that intervals
```


### Chords:

```
application will find basic chords in given key in different scales
```
